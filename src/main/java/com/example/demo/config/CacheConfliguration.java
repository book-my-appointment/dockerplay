package com.example.demo.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.GenericToStringSerializer;


@Configuration
public class CacheConfliguration {
	@Value("${spring.redis.host}")
	private String redisHost;
	
	
	@Value("${spring.redis.port}")
	private Integer redisPort;
	
	@Value("${spring.redis.password}")
	private String redisPassword;
	
	@Bean
	JedisConnectionFactory jedisConnectionFactory() {
		// RedisStandaloneConfiguration redisStandaloneConfiguration = new
		// RedisStandaloneConfiguration("redis-server", 6379);
		RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration(redisHost, redisPort);
		redisStandaloneConfiguration.setPassword(redisPassword);
		return new JedisConnectionFactory(redisStandaloneConfiguration);
	}

	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setEnableTransactionSupport(true);
		template.setConnectionFactory(jedisConnectionFactory());
		template.setValueSerializer(new GenericToStringSerializer<Object>(Object.class));
		return template;
	}
}
