package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.CacheFlags;
import com.example.demo.service.CacheFlagService;

@RestController
public class CacheFlagController {

	@Autowired
	private CacheFlagService cacheFlagService;
	
	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	@GetMapping("/startup/{userId}")
	@Cacheable(value = "users", key = "#userId", unless = "#userId < 12000")
	public String startup(@PathVariable Integer userId) {
		System.out.print("Hello" + userId);
		return "Hello000 123 456 789 101112 " + userId;
	}
	
	

	@PostMapping("/cacheFlags/{key}/{value}")
	public ResponseEntity<CacheFlags> saveCacheFlag(@PathVariable("key") String key,
			@PathVariable("value") String value) {
		System.out.println("in main code");
		redisTemplate.opsForValue().set(key, value);
		return new ResponseEntity<CacheFlags>(cacheFlagService.saveCacheFlags(key, value), HttpStatus.CREATED);
	}

}
