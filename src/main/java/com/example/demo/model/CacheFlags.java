package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "cache_flags")
@NoArgsConstructor
@AllArgsConstructor
public class CacheFlags implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8396154942979240902L;

	@Id
	@Column(name = "CACHE_KEY")
	private String key;

	@Column(name = "CACHE_VALUE",nullable = false, columnDefinition = "BIT", length = 1)
	private String value;

}
