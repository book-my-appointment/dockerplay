package com.example.demo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.CacheFlags;
import com.example.demo.service.inf.CacheFlagsServiceInf;
import com.example.demo.service.repo.CacheFlagRepository;


@Service
public class CacheFlagService implements CacheFlagsServiceInf {

	@Autowired
	private CacheFlagRepository cacheFlagRepo;
	
	@Override
	public CacheFlags saveCacheFlags(String key, String value) {
		CacheFlags cf = new CacheFlags(key, value);
		return cacheFlagRepo.save(cf);
	}

}
