package com.example.demo.service.inf;

import com.example.demo.model.CacheFlags;

public interface CacheFlagsServiceInf {
	CacheFlags saveCacheFlags(String key, String value);
}
