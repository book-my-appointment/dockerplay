package com.example.demo.service.repo;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.model.CacheFlags;


@Repository
@Transactional
public interface CacheFlagRepository extends JpaRepository<CacheFlags,String>{

}
